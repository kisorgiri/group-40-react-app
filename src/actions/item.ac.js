import { errorHandler } from "../services/error.service"
import { httpClient } from "../utils/httpClient"

export const ItemActionTypes = {
  SET_IS_LOADING: 'SET_IS_LOADING',
  ITEMS_RECEIVED: 'ITEMS_RECEIVED'
}

// create action here
// export const fetchItems_ac=(params)=>{
//   // this function return a object which will be passed to reducer  
//   return function(dispatch){
//     // dispatch will be called and it will transfer its control to reducer
//   }
// }

// function fetchItems_ac(condition) {
//   return function (dispatch) {

//   }
// }

export const fetchItems_ac = params => dispatch => {
  console.log('at actions >>', params)
  dispatch({
    type: ItemActionTypes.SET_IS_LOADING,
    payload: true
  })

  httpClient.GET('/item', true, params)
    .then(({ data }) => {
      dispatch({
        type: ItemActionTypes.ITEMS_RECEIVED,
        payload: data
      })
    })
    .catch(err => {
      errorHandler(err);
    })
    .finally(() => {
      dispatch({
        type: ItemActionTypes.SET_IS_LOADING,
        payload: false
      })
    })
}
