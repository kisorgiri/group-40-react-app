import { createStore, applyMiddleware } from 'redux';

import thunk from 'redux-thunk';
import { rootReducer } from './reducers';

const midllewares = [thunk]

const defaultState = {
  item: {
    items: [],
    isLoading: false
  }
};

export const store = createStore(rootReducer, defaultState, applyMiddleware(...midllewares))


// {
//   item:{},
//   items:[],
//   itemLoading:false,
//   itemSUbmitting:false,
//   user:{},
//   userData:[],
//   uyseIsLoading:false,
//   notification
// }

// {
//   item: {
//     items: [],
//       isLoading: false,
//         isSubmitting: false
//   },
//   notification: {
//     isLoading: false,
//       data: []
//   },
//   users: {
//     data: [],
//       loading: false
//   }
// }