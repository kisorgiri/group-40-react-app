import React, { useState, useEffect } from 'react';
import { formatDate } from '../../../utils/dateUtil';
import { SubmitButton } from './../../common/submitButton/submitButton.component'
import { FaTrashAlt } from 'react-icons/fa'

const IMG_URL = process.env.REACT_APP_IMG_URL;

const defaultFormFields = {
  name: '',
  description: '',
  brand: '',
  price: '',
  color: '',
  quantity: '',
  weight: '',
  size: '',
  category: '',
  modelNo: '',
  specificiations: '',
  purchasedDate: '',
  salesDate: '',
  returnedDate: '',
  isReturnEligible: '',
  discountedItem: '',
  discountType: '',
  discountValue: '',
  tags: '',
}

export const ItemForm = (props) => {

  const [formFields, setFormFields] = useState({
    ...defaultFormFields
  })
  const [filesToUpload, setFilesToUpload] = useState([]);
  const [filesToPreview, setFilesToPreview] = useState([]); // showing existing images
  const [filesToRemove, setFilesToRemove] = useState([]); // state for removing existin files

  useEffect(() => {
    // only for edit
    if (props.itemData) {
      let discountProp = props.itemData.discount;
      setFormFields({
        ...defaultFormFields,
        ...props.itemData,
        discountedItem: discountProp && discountProp.discountedItem ? discountProp.discountedItem : '',
        discountType: discountProp && discountProp.discountType ? discountProp.discountType : '',
        discountValue: discountProp && discountProp.discountValue ? discountProp.discountValue : '',
        purchasedDate: props.itemData.purchasedDate ? formatDate(props.itemData.purchasedDate, 'YYYY-MM-DD') : '',
        salesDate: props.itemData.salesDate ? formatDate(props.itemData.salesDate, 'YYYY-MM-DD') : '',
        returnedDate: props.itemData.returnedDate ? formatDate(props.itemData.returnedDate, 'YYYY-MM-DD') : ''
      })
      setFilesToPreview(props.itemData.images);
    }

  }, [])

  const handleChange = e => {
    let { name, value, type, checked, files } = e.target;
    if (type === 'checkbox') {
      value = checked;
    }

    if (type === 'file') {
      const newFilesToUpload = [...filesToUpload];
      newFilesToUpload.push(files[0]);

      setFilesToUpload(newFilesToUpload);
      return
    }
    setFormFields({
      ...formFields,
      [name]: value
    })
  }

  const handleSubmit = e => {
    e.preventDefault();
    const requestData = {
      ...formFields,
      filesToRemove: filesToRemove
    }
    props.submitCallback(requestData, filesToUpload);
  }

  const removeUploadSelection = (index) => {
    const existingFiles = [...filesToUpload]
    existingFiles.splice(index, 1)
    setFilesToUpload(existingFiles);
  }

  const removeExistingImages = (item, index) => {
    console.log('item is >', item)
    const removeContent = [...filesToRemove];
    removeContent.push(item);

    setFilesToRemove(removeContent);

    // remove from state
    let existingFiles = [...filesToPreview]
    existingFiles.splice(index, 1);
    setFilesToPreview(existingFiles)
  }

  return (
    <>
      <h2>{props.title}</h2>
      <form className="form-group" onSubmit={handleSubmit} noValidate>
        <label>Name</label>
        <input type="text" name="name" value={formFields.name} placeholder="Name" onChange={handleChange} className="form-control"></input>
        <label>Description</label>
        <textarea rows="5" name="description" value={formFields.description} placeholder="Description" onChange={handleChange} className="form-control"></textarea>
        <label>Category</label>
        <input type="text" name="category" value={formFields.category} placeholder="Category" onChange={handleChange} className="form-control"></input>
        <label>Brand</label>
        <input type="text" name="brand" value={formFields.brand} placeholder="Brand" onChange={handleChange} className="form-control"></input>
        <label>Price</label>
        <input type="text" name="price" value={formFields.price} placeholder="Price" onChange={handleChange} className="form-control"></input>
        <label>Color</label>
        <input type="text" name="color" value={formFields.color} placeholder="Color" onChange={handleChange} className="form-control"></input>
        <label>Quantity</label>
        <input type="number" name="quantity" value={formFields.quantity} onChange={handleChange} className="form-control"></input>
        <label>Weight</label>
        <input type="text" name="weight" value={formFields.weight} placeholder="Weight" onChange={handleChange} className="form-control"></input>
        <label>Size</label>
        <input type="text" name="size" value={formFields.size} placeholder="Size" onChange={handleChange} className="form-control"></input>
        <label>Model No</label>
        <input type="text" name="modelNo" value={formFields.modelNo} placeholder="Model No" onChange={handleChange} className="form-control"></input>
        <label>Specifications</label>
        <input type="text" name="specificiations" value={formFields.specificiations} placeholder="Specifications" onChange={handleChange} className="form-control"></input>
        <label>Purchased Date</label>
        <input type="date" name="purchasedDate" value={formFields.purchasedDate} onChange={handleChange} className="form-control"></input>
        <label>Sales Date</label>
        <input type="date" name="salesDate" value={formFields.salesDate} onChange={handleChange} className="form-control"></input>
        <label>Returned Date</label>
        <input type="date" name="returnedDate" value={formFields.returnedDate} onChange={handleChange} className="form-control"></input>
        <input type="checkbox" checked={formFields.isReturnEligible} name="isReturnEligible" onChange={handleChange}></input>
        <label> &nbsp;Return Eligible</label>
        <br />
        <input type="checkbox" checked={formFields.discountedItem} name="discountedItem" onChange={handleChange}></input>
        <label> &nbsp;Discounted Item</label>
        <br />
        {
          formFields.discountedItem && (
            <>
              <label>Discount Type</label>
              <select name="discountType" value={formFields.discountType} onChange={handleChange} className="form-control">
                <option value="">(Select Type)</option>
                <option value="percentage">Percentage</option>
                <option value="quantity">QTY</option>
                <option value="value">Value</option>
              </select>
              <label>Discount Value</label>
              <input type="text" name="discountValue" value={formFields.discountValue} placeholder="Discount Value" onChange={handleChange} className="form-control"></input>
            </>
          )
        }
        <label>Tags</label>
        <input type="text" name="tags" value={formFields.tags} placeholder="Tags" onChange={handleChange} className="form-control"></input>
        {
          filesToPreview.length > 0 && (
            <>
              <label>Existing Images:</label>
              {filesToPreview.map((item, index) => (
                <div style={{ marginTop: '5px' }} key={index}>
                  <img src={`${IMG_URL}/${item}`} alt="upload.png" width="200px"></img>
                  <span onClick={() => removeExistingImages(item, index)} title="remove file" style={{ marginLeft: '5px', cursor: 'pointer' }}>
                    <FaTrashAlt color='red'></FaTrashAlt>
                  </span>
                </div>
              ))
              }
            </>
          )
        }
        <label>Choose Image(s)</label>
        <input type="file" className="form-control" onChange={handleChange}></input>
        {
          filesToUpload.length > 0 && filesToUpload.map((item, index) => (
            <div style={{ marginTop: '5px' }} key={index}>
              <img src={URL.createObjectURL(item)} alt="upload.png" width="200px"></img>
              <span onClick={() => removeUploadSelection(index)} title="remove file" style={{ marginLeft: '5px', cursor: 'pointer' }}>
                <FaTrashAlt color='red'></FaTrashAlt>
              </span>
            </div>
          ))
        }

        <hr />
        <SubmitButton
          isSubmitting={props.isSubmitting}
        >

        </SubmitButton>

      </form>
    </>
  )
}