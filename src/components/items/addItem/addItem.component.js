import React, { useState } from 'react';
import { ItemForm } from '../itemForm/itemForm.component';
import { httpClient } from './../../../utils/httpClient'
import { errorHandler } from './../../../services/error.service'
import { notify } from '../../../utils/notify';
import { useNavigate } from 'react-router-dom'

export const AddItem = (props) => {
  const navigate = useNavigate();
  const [isSubmitting, setIsSubmitting] = useState(false);

  const add = (data, files) => {
    setIsSubmitting(true);
    httpClient.UPLOAD('POST', '/item', data, files)
      .then(response => {
        notify.showInfo("Item Added successfully")
        navigate('/view_items')

      })
      .catch(err => {
        setIsSubmitting(false)
        errorHandler(err);
      })
  }
  return (
    <ItemForm
      title="Add Item"
      submitCallback={add}
      isSubmitting={isSubmitting}
    ></ItemForm>
  )
}