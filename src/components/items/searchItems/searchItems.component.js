import React, { Component } from 'react';
import { errorHandler } from '../../../services/error.service';
import { httpClient } from '../../../utils/httpClient';
import { notify } from '../../../utils/notify';
import { SubmitButton } from '../../common/submitButton/submitButton.component';
import { ViewItems } from './../viewItems/viewItems.component'

const defautlFormFields = {
  name: '',
  category: '',
  minPrice: '',
  maxPrice: '',
  fromDate: '',
  toDate: '',
  tags: '',
  multipleDateRange: false
}
export class SearchItems extends Component {
  constructor(props) {
    super(props)

    this.state = {
      data: {
        ...defautlFormFields
      },
      error: {
        ...defautlFormFields
      },
      allItems: [],
      categories: [],
      names: [],
      searchResults: [],
      isSubmitting: false,

    }
  }

  componentDidMount() {
    httpClient.POST('/item/search', {})
      .then(({ data }) => {
        let cats = [];
        data.forEach((item, index) => {
          if (!cats.includes(item.category)) {
            cats.push(item.category)
          }
        })
        this.setState({
          categories: cats,
          allItems: data
        })
      })
      .catch(err => {
        errorHandler(err)
      })
  }

  handleChange = e => {
    let { name, value, type, checked } = e.target;

    if (name === 'category') {
      this.filterNames(value)
    }
    if (type === 'checkbox') {
      value = checked;
    }

    this.setState(preState => ({
      data: {
        ...preState.data,
        [name]: value
      }
    }))
  }

  filterNames = selectedCat => {
    const { allItems } = this.state;
    let names = allItems.filter((item, index) => {
      if (item.category === selectedCat) {
        return true;
      }
    })
    this.setState({
      names: names
    })
  }

  handleSubmit = e => {
    e.preventDefault();
    this.setState({
      isSubmitting: true
    })
    const { data } = this.state;
    if (!data.multipleDateRange) {
      data.toDate = data.fromDate
    }
    httpClient.POST(`/item/search`, data)
      .then(({ data }) => {
        if (!data.length) {
          return notify.showInfo("No Any Items Matched Your Search Query")
        }
        this.setState({
          searchResults: data
        })
      })
      .catch(err => {
        errorHandler(err);
      })
      .finally(() => {
        this.setState({
          isSubmitting: false
        })
      })
  }

  reset = () => {
    this.setState({
      searchResults: [],
      data: {
        ...defautlFormFields
      }
    })
  }

  render() {
    let content = this.state.searchResults && this.state.searchResults.length > 0
      ? <ViewItems resetSearch={this.reset} searchResults={this.state.searchResults}></ViewItems>
      : <>
        <h2>Search Products</h2>
        <form onSubmit={this.handleSubmit} className="form-group">
          <label>Category</label>
          <select className="form-control" value={this.state.data.category} name="category" onChange={this.handleChange}>
            <option value="">(Select Category)</option>
            {
              this.state.categories.map((cat, index) => (
                <option value={cat}>{cat}</option>
              ))
            }
          </select>
          {
            this.state.names && this.state.names.length > 0 && (
              <>
                <label>Name</label>
                <select className="form-control" value={this.state.data.name} name="name" onChange={this.handleChange}>
                  <option value="">(Select Name)</option>
                  {
                    this.state.names.map((nm, index) => (
                      <option value={nm.name}>{`${nm.name}(${nm.brand})`}</option>
                    ))
                  }
                </select>
              </>
            )
          }

          <label>Min Price</label>
          <input type="number" name="minPrice" onChange={this.handleChange} className="form-control"></input>
          <label>Max Price</label>
          <input type="text" name="maxPrice" onChange={this.handleChange} className="form-control"></input>
          <label>Select Date</label>
          <input type="date" name="fromDate" onChange={this.handleChange} className="form-control"></input>
          <input type="checkbox" name="multipleDateRange" onChange={this.handleChange}></input>
          <label> &nbsp;Multiple Date Range</label>
          {
            this.state.data.multipleDateRange && (
              <>
                <br />
                <label>To Date</label>
                <input type="date" name="toDate" onChange={this.handleChange} className="form-control"></input>
              </>
            )
          }
          <br />
          <label>Tags</label>
          <input type="text" name="tags" placeholder="Tags" onChange={this.handleChange} className="form-control"></input>
          <hr />
          <SubmitButton
            isSubmitting={this.state.isSubmitting}
          ></SubmitButton>

        </form>
      </>
    return content
  }
}
