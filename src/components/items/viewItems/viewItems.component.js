import React, { Component } from 'react';
import { errorHandler } from '../../../services/error.service';
import { httpClient } from '../../../utils/httpClient';
import { FaPencilAlt, FaTrashAlt } from 'react-icons/fa'
import { notify } from '../../../utils/notify';
import { Link } from 'react-router-dom';
import { Loader } from '../../common/loader/loader.component';
import { formatDate } from '../../../utils/dateUtil';
import { connect } from 'react-redux';
import { fetchItems_ac } from '../../../actions/item.ac';


const IMG_URL = process.env.REACT_APP_IMG_URL;
class ViewItemsComponent extends Component {
  constructor() {
    super();
  }

  async componentDidMount() {
    console.log('this.props >>', this.props)
    if (this.props.searchResults) {
      return this.setState({
        items: this.props.searchResults
      })
    }
    this.props.fetchItems({
      a: 'b'
    });
    // this.setState({
    //   isLoading: true
    // })
    // try {
    //   let { data } = await httpClient.GET('/item', true);
    //   this.setState({
    //     items: data
    //   })
    // }
    // catch (err) {
    //   errorHandler(err);
    // }
    // finally {
    //   this.setState({
    //     isLoading: false
    //   })
    // }
  }

  editItem = (id) => {
    // navigate('/')
  }

  // TRY remove ko cycle from redux
  removeItem = (id, index) => {
    let confirmation = window.confirm('Are you sure to remove?');
    if (confirmation) {
      httpClient.DELETE(`/item/${id}`, true)
        .then(response => {
          notify.showSuccess("Item Removed")
          const { items } = this.state;
          items.splice(index, 1);

          this.setState({
            items: items
          })
        })
        .catch(err => {
          errorHandler(err);
        })

    }
  }

  render() {
    let content = this.props.isLoading
      ? <Loader></Loader>
      : <>
        <h2>View Items</h2>
        {
          this.props.searchResults && this.props.searchResults.length > 0 && (
            <button className="btn btn-success" onClick={() => this.props.resetSearch()}>Search Again</button>
          )
        }
        <table className="table table-bordered">
          <thead>
            <tr>
              <th>S.N</th>
              <th>Name</th>
              <th>Category</th>
              <th>Price</th>
              <th>Brand</th>
              <th>Created At</th>
              <th>Tags</th>
              <th>Images</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {(this.props.items || []).map((item, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td> <Link to={`/item_details/${item._id}`}> {item.name}</Link></td>
                <td>{item.category}</td>
                <td>{item.price}</td>
                <td>{item.brand}</td>
                <td>{formatDate(item.createdAt)}</td>
                <td>{item.tags.join(',')}</td>
                <td>
                  <img src={`${IMG_URL}/${item.images[0]}`} alt="itemImage.png" width="150px"></img>
                </td>
                <td>
                  <Link to={`/edit_item/${item._id}`}> <FaPencilAlt onClick={() => this.editItem(item._id)} title="Edit" color="blue"></FaPencilAlt></Link>  |
                  <FaTrashAlt onClick={() => this.removeItem(item._id, index)} title="Remove" color="red"></FaTrashAlt>
                </td>
              </tr>
            ))}




          </tbody>
        </table>
      </>
    return content;
  }
}

// map state to props ==> incoming data from store
const mapStateToProps = rootStore => {
  return {
    items: rootStore.item.items,
    isLoading: rootStore.item.isLoading,
  }
}

// outgoin actions from your conmponent
const mapDispatchToProps = dispatch => ({
  fetchItems: (cond) => dispatch(fetchItems_ac(cond))
})

export const ViewItems = connect(mapStateToProps, mapDispatchToProps)(ViewItemsComponent)