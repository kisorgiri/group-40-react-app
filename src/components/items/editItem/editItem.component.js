import React, { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom'
import { errorHandler } from '../../../services/error.service';
import { httpClient } from '../../../utils/httpClient';
import { notify } from '../../../utils/notify';
import { Loader } from '../../common/loader/loader.component';
import { ItemForm } from '../itemForm/itemForm.component';

export const EditItem = (props) => {

  const [isLoading, setIsLoading] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [item, setItem] = useState({});

  let navigate = useNavigate();
  let params = useParams();


  useEffect(() => {
    setIsLoading(true)
    httpClient.GET(`/item/${params.id}`, true)
      .then(({ data }) => {
        setItem(data);
      })
      .catch(err => {
        errorHandler(err);
      })
      .finally(() => {
        setIsLoading(false)
      });
  }, [])

  const edit = (data, files) => {
    // edited data from item form here
    console.log('data in edit >>', data)
    setIsSubmitting(true)
    httpClient.UPLOAD('PUT', `/item/${params.id}`, data, files)
      .then(response => {
        notify.showInfo('Item Updated successfully');
        navigate('/view_items')
      })
      .catch(err => {
        console.log('error is >>', err)
        errorHandler(err)
        setIsSubmitting(false)
      })

  }

  let content = isLoading
    ? <Loader></Loader>
    : <ItemForm
      title="Edit Item"
      submitCallback={edit}
      itemData={item}
      isSubmitting={isSubmitting}
    >
    </ItemForm>

  return content;
}