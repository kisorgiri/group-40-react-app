// library/packages used ==> react router dom
// v 6 
// in v6 it has major changes incompare to v5
// v 6 (latest ) approach

import React, { Component, useEffect } from 'react';
import { BrowserRouter, Routes, Route, useParams, useLocation, useNavigate } from 'react-router-dom'
import { Login } from './auth/login/login.component';
import { Register } from './auth/register/register.component';
import { Header } from './../components/common/header/header.component'
import { getAuthenticationToken } from './../utils'
import { Sidebar } from './common/sidebar/sidebar.component';
import { AddItem } from './items/addItem/addItem.component';
import { EditItem } from './items/editItem/editItem.component';
import { ViewItems } from './items/viewItems/viewItems.component';
import { SearchItems } from './items/searchItems/searchItems.component';
import { ForgotPassword } from './auth/forgotPassword/forgotPassword.component';
import { ResetPassword } from './auth/resetPassword/resetPassword.component';


const Home = (props) => {
  console.log('props in home is >>', props)
  return <p>Home Page</p>
}

const Dashboard = (props) => {
  console.log('props in dashboard is >>', props)
  let params = useParams();
  return (
    <>
      <h3>Welcome to Group 40 Web Store </h3>
      <p> Please us side navigation menu or contact system administrator for support</p>
    </>
  )
}

const About = (props) => {
  const loc = useLocation();
  console.log('props in About is >>', loc)
  return <p>About Page</p>
}

const Contact = (props) => {
  console.log('props in Contact is >>', props)
  return <p>Contact Page</p>
}

const Settings = (props) => {
  console.log('props in Settings is >>', props)
  return <p>Settings Page</p>
}

const NotFound = (props) => {
  console.log('props in NotFound is >>', props)
  return (
    <div>
      <p>Not Found</p>
      <img src='./images/notfound.png' width="600px"></img>
    </div>
  )
}

const ProtectedRoute = ({ component: Component, ...rest }) => {
  let navigate = useNavigate();

  useEffect(() => {
    let token = getAuthenticationToken();
    if (!token) {
      navigate('/');
    }
  }, [])


  return (
    <>
      <Header isLoggedIn={true}></Header>
      <Sidebar></Sidebar>
      <div className="main_content">
        <Component></Component>

      </div>
    </>
  )
}


const PublicRoute = ({ component: Component, ...rest }) => {

  return (
    <>
      <Header isLoggedIn={getAuthenticationToken() ? true : false}></Header>
      <Component />

    </>
  )

}

const AuthRoute = ({ component: Component, ...rest }) => {

  return (
    <>
      <Header isLoggedIn={false}></Header>
      <Component />

    </>
  )

}

export const AppRouting = () => {

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<AuthRoute component={Login} />}></Route>
        <Route path="/register" element={<AuthRoute component={Register} />}></Route>
        <Route path="/forgot_password" element={<AuthRoute component={ForgotPassword} />}></Route>
        <Route path="/reset_password/:id" element={<AuthRoute component={ResetPassword} />}></Route>
        <Route path="/about" element={<PublicRoute component={About} />}></Route>
        <Route path="/contact" element={<PublicRoute component={Contact} />}></Route>
        <Route path="/settings" element={<ProtectedRoute component={Settings} />}></Route>
        <Route path="/home" element={<PublicRoute component={Home} />}></Route>
        <Route path="/dashboard" element={<ProtectedRoute component={Dashboard} />}></Route>
        <Route path="/add_item" element={<ProtectedRoute component={AddItem} />}></Route>
        <Route path="/view_items" element={<ProtectedRoute component={ViewItems} />}></Route>
        <Route path="/search_items" element={<ProtectedRoute component={SearchItems} />}></Route>
        <Route path="/edit_item/:id" element={<ProtectedRoute component={EditItem} />}></Route>
        <Route path="*" element={<PublicRoute component={NotFound} />}></Route>
      </Routes>
    </BrowserRouter>
  )


}

// react-router-dom v6
// BrowserRouter
// Routes
// Route ==> main configuration
// path and element attribute are used

// Link is used to navigate on click event

// important hooks
// useNavigate ==> navigation
// useParams ==> part of url ko value
// useLocation ==> optional url value 

// notfound ko configuration path ='*'