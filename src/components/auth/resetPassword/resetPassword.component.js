import React, { useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { errorHandler } from '../../../services/error.service';
import { httpClient } from '../../../utils/httpClient';
import { notify } from '../../../utils/notify';
import { SubmitButton } from '../../common/submitButton/submitButton.component';

const defaultFormFields = {
  password: '',
  confirmPassword: ''
}

export const ResetPassword = (props) => {
  const [isSubmitting, setIsSubmitting] = useState(false)
  const [formFields, setFormFields] = useState({ ...defaultFormFields });

  const navigate = useNavigate();
  const params = useParams();

  const submit = e => {
    e.preventDefault();
    setIsSubmitting(true);
    httpClient.POST(`/auth/reset-password/${params.id}`, formFields)
      .then(response => {
        notify.showSuccess("Password Reset Successfull please login");
        navigate('/');
      })
      .catch(err => {
        setIsSubmitting(false)
        errorHandler(err)
      })
  }
  const change = e => {
    let { name, value } = e.target;
    setFormFields({
      ...formFields,
      [name]: value
    })
  }
  return (
    <div className="auth_box">
      <h2>Reset Password</h2>
      <p>Please Choose your password</p>
      <form className="form-group" onSubmit={submit}>
        <label>Password</label>
        <input type="password" name="password" onChange={change} placeholder="Password...." className="form-control"></input>
        <label>Confirm Password</label>
        <input type="password" name="confirmPassword" onChange={change} placeholder="Confirm Password...." className="form-control"></input>
        <br />
        <SubmitButton
          isSubmitting={isSubmitting}
        ></SubmitButton>

      </form>
      <p>Back to <Link to="/">Login</Link></p>
    </div>
  )
}