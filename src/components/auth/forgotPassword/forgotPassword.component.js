import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { errorHandler } from '../../../services/error.service';
import { httpClient } from '../../../utils/httpClient';
import { notify } from '../../../utils/notify';
import { SubmitButton } from '../../common/submitButton/submitButton.component';

export const ForgotPassword = (props) => {
  const [isSubmitting, setIsSubmitting] = useState(false)
  const [email, setEmail] = useState('');
  const navigate = useNavigate();
  const submit = e => {
    e.preventDefault();
    setIsSubmitting(true);
    httpClient.POST('/auth/forgot-password', { email })
      .then(response => {
        notify.showSuccess("Password Reset Link sent!, please check your inbox");
        navigate('/');
      })
      .catch(err => {
        setIsSubmitting(false)
        errorHandler(err)
      })
  }
  const change = e => {
    setEmail(e.target.value);
  }
  return (
    <div className="auth_box">
      <h2>Forgot Password</h2>
      <p>Please use your email address to reset your password</p>
      <form className="form-group" onSubmit={submit}>
        <label>Email</label>
        <input type="text" name="email" onChange={change} placeholder="Email Address...." className="form-control"></input>
        <br />
        <SubmitButton
          isSubmitting={isSubmitting}
        ></SubmitButton>

      </form>
      <p>Back to <Link to="/">Login</Link></p>
    </div>
  )
}