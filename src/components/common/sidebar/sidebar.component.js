import React from 'react';
import { Link } from 'react-router-dom';


import './sidebar.component.css';

export const Sidebar = (props) => {

  return (
    <div className="sidebar">
      <Link to="/add_item">Add Item</Link>
      <Link to="/view_items">View Items</Link>
      <Link to="/search_items">Search Items</Link>
      <Link to="/notifications">Notifications</Link>
      <Link to="/messages">Messages</Link>
    </div>
  )
}