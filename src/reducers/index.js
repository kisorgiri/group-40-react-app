import { combineReducers } from 'redux';
import { itemReducer } from './item.red';

export const rootReducer = combineReducers({
  item: itemReducer,
})