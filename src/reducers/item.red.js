import { ItemActionTypes } from "../actions/item.ac";

const defaultState = {
  items: [],
  isLoading: false
}

export const itemReducer = (state = defaultState, action) => {
  console.log('at reducer with action >>', action);
  switch (action.type) {
    case ItemActionTypes.SET_IS_LOADING:
      return {
        ...state,
        isLoading: action.payload
      }

    case ItemActionTypes.ITEMS_RECEIVED:
      return {
        ...state,
        items: action.payload
      }

    default:
      return {
        ...state
      }
  }

}