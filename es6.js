// 1. Object Shorthand
// 2. Object destruction
// 3. spread operator and rest operator
// 4. arrow notation function
// 5. template literals
// 6. default arguments
// 7. import and export
// 8. block scope
// 9. class 

// var phone = 333;


// var details = {
//   name: 'test',
//   addr: 'bkt',
//   phone
// }

// console.log('details >>,', details)

// Object destruction

// function sendMail({ subject, sender }) {
//   console.log('subject is >>', subject)
//   console.log('sender is >>', sender)
// }

// sendMail({
//   sender: 'ram',
//   receiver: 'shyam',
//   subject: 'hello',
//   message: 'hi'
// })

// var res = {
//   sender: 'ram',
//   receiver: 'shyam',
//   subject: 'hello',
//   message: 'hi'
// }

// const { message } = res;
// console.log('message >>', message)
// var color = 'yellow';

// function getSomething() {
//   return {
//     fruits: 'apple',
//     price: 33,
//     color: 'red',
//     staus: 'sold'
//   }
// }

// var { fruits, color: myColor } = getSomething();

// console.log('color >>', color)
// console.log('mycolor', myColor)

// spread operator and rest operator 
// ...

// var obj = {
//   status: 'sold',
//   price: 333
// }
// var abc = {
//   color: 'red',
//   price: 9999,
//   description: 'good'
// }

// var finalObj = {
//   ...obj,
//   ...abc,
//   brand:'test'
// }
// var obj2 = { ...obj };
// obj2.status = 'booked'

// console.log('obj ', obj);
// console.log('obj2', obj2)
// console.log('final obj >>>', finalObj)

// rest operator
// function getSomething() {
//   return {
//     fruits: 'apple',
//     price: 33,
//     color: 'red',
//     staus: 'sold'
//   }
// }
// var { fruits: Random, ...remaining } = getSomething();
// console.log('Random >>>', Random);
// console.log('and remaining is >>', remaining)

// Arrow notation function
// function ==> reusable block of code
// expression syntax function
// declaration syntax function
// arrow notation function

// syntax of arrow notation function
// function welcome(){
// // declaration
// }

// var welcome= function(){
// // expression
// }

// var welcome =()=>{
// // arrow notation
// }

// 1. for single argument () is optional

// 2. arrow notation function can exclude { } braces
//      3. arrow notaiton function can exclude use of return keyword

// IMP ==> arrow notation function will inherit paren't this

// var hello = name=>{
//   return 'hi '+ name;
// }
// one liner function
// var hello = name => 'hi ' + name+ ' welcome to broadway';
// template literals
var hello = name => `lsdkjf
dlkjf
lkdjf
${name} wleocme to brodway`;

console.log('hello >>', hello('bikram'))

var cycles = [{
  type: 'allmountatin',
  color: 'red',
  brand: 'giant'
},
{
  type: 'hardtrail',
  color: 'black',
  brand: 'cube'
},
{
  type: 'hardtrail',
  color: 'blue',
  brand: 'giant'
}]

var giantCycles = cycles.filter(function (item) {
  if (item.brand === 'giant') {
    return true;
  }
})

var giantCyclesArrowNotation = cycles.filter(item => item.brand === 'giant')

console.log('giant cycles >>', giantCycles)
console.log('giant with arrow cycles >>', giantCyclesArrowNotation)

// template literals
// syntax `` with ${}

// default arguments

// function sendMail(arg='nepal') {
//   console.log('arg val', arg)
// }

// sendMail({});


// import and export

// export 
// two ways of export
// named export
// there can be multiple named export from a file
// syntax 
// export keyword
// eg.
// export const A = 'val';
// export class Ram {}

// default export
// there can be only one default export from afile
// syntax 
// default export
// default export anyvalue

// there can be multile named and a default export in a file

// import 

// Imort totatally depends on how it is exported
// if named export
// import syntax
// import {named1,named2} from './path to source file'


// if default export 
// import anyname from './path_to_source file'

// if both
// import ANYNAME,{name1,name2 from 'path to source file'}

// block scope 
// let keyword

// class ==> group of constructor, fields(properties), and methods

// eg 
class Books {
  genre;
  author;
  constructor() {
    this.genre = 'programming';
    this.author = 'ryan'
  }

  getGenre() {
    
    return this.genre;

  }

  setGenre(newGenrre) {
    this.genre = newGenrre
    return this.genre;
  }


}

// inheritance
class Pen extends Books {
  origin;
  constructor() {
    super(); // super call is parent class constructor call
    this.origin = 'nepal'
  }
}