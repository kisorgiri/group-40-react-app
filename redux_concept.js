// redux ===> state management tool for JS app

// web architecture

// MVC==> concerns ==> Models, Views, Controllers
// bidirectional data flow betwenn models and controller


// FLUX architecture
// concerns ===> View, Actions, Dispatcher, Store

// unidirectional data flow
// views (react component) ==> Actions ===> Dispatcher===> store 
// FLUX ==> there can be multiple store


// REDUX 
// Redux is built on top of flux
// concerns===> Views, Actions, Reducers, Store

// Unidirectional data flow
// single source of truth
// Views ===> Actions===> Reducers ==> store

// Views ==> react component
// Actions ==> Action defination
// Reducer ==> holds logic to update store
// store ==> contain application data